import React from 'react';
import { StyleSheet, View } from 'react-native';

interface BackgroundProps {
  children: React.ReactNode;
}

const Background = (props: BackgroundProps) => {
  return <View style={styles.container}>{props.children}</View>;
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgrey',
    margin: 10,
    opacity: 0.5,
    borderRadius: 8,
  },
});

export default Background;
