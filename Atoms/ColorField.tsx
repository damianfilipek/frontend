import React from 'react';
import { StyleSheet, View } from 'react-native';

interface BackgroundProps {
  children: React.ReactNode;
}

const Background = (props: BackgroundProps) => {
  return <View style={styles.container}>{props.children}</View>;
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default Background;
