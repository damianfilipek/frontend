const date1 = new Date(2021, 12, 1);
const date2 = new Date(2021, 12, 2);
const date3 = new Date(2021, 12, 3);
const date8 = new Date(2021, 12, 4);

export const Api = {
  getProgress: [
    {
      day: 1,
      date: '01.12.2021',
      text: 'day 1',
    },
    {
      day: 2,
      date: '02.12.2021',
      text: 'day 2',
    },
    {
      day: 3,
      date: '03.12.2021',
      text: 'day 3',
    },
    {
      day: 4,
      date: '04.12.2021',
      text: 'day 4',
    },
  ],
};
