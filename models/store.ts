import { types } from 'mobx-state-tree';
import { createContext, useContext } from "react";

export const Store = types.model('store', {
  // charactersList: types.optional(CharactersList, { characters: [], state: 'done'}),
});

export const store = Store.create({})

const RootStoreContext = createContext(null);

export const Provider = RootStoreContext.Provider;
export function useMst() {
  const store = useContext(RootStoreContext);
  if (store === null) {
    throw new Error("Store cannot be null, please add a context provider");
  }
  return store;
}