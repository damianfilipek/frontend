import { types } from 'mobx-state-tree';
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';

const Data = types.model('Data', {
  name: types.string,
  value: types.string,
  id: types.string,
});

const Images = types.model('Images', {
  uri: types.string,
  width: types.number,
  height: types.number,
  id: types.string,
  cancelled: types.boolean,
});

const Measurement = types.model('Measurement', {
  data: types.array(Data),
  id: types.string,
  date: types.Date,
  images: types.array(Images),
});

export const Measurements = types
  .model('Measurements', {
    measurements: Measurement,
  })
  .actions((self) => ({
    addMeasurement() {
      self.measurements.data.push({ name: '', value: '', id: uuidv4() });
    },
    deleteMeasurement(data: any) {
      const index = self.measurements.data.indexOf(data);
      self.measurements.data.splice(index, 1);
    },
    changeName(data: { name: string; id: string }) {
      self.measurements.data.find((el) => {
        if (el.id === data.id) {
          el.name = data.name;
        }
      });
    },
    changeValue(data: { value: string; id: string }) {
      self.measurements.data.find((el) => {
        if (el.id === data.id) {
          el.value = data.value;
        }
      });
    },
    addImage(data: {
      uri: string;
      width: number;
      height: number;
      cancelled: boolean;
    }) {
      self.measurements.images.push({ ...data, id: uuidv4() });
    },
    removeImage(data: {
      uri: string;
      width: number;
      height: number;
      id: string;
      cancelled: boolean;
    }) {
      const index = self.measurements.images.indexOf(data);
      self.measurements.images.splice(index, 1);
    },
  }))
  .create({
    measurements: {
      data: [{ name: '', value: '', id: uuidv4() }],
      id: uuidv4(),
      date: new Date(),
      images: [],
    },
  });

export default Measurements;
