import React from 'react';
import { SafeAreaView, ImageBackground } from 'react-native';
import Login from './components/views/Login';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import BackgroundLayout from './components/templates/BackgroundLayout';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import CreateProgress from './components/views/CreateProgress';
import 'react-native-get-random-values';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <SafeAreaProvider style={{ backgroundColor: 'rgba(0,0,0,0.1)', height: '100%' }}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="CreateProgress"
            component={CreateProgress}
            options={{ headerShown: true }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
