import React, { Children } from 'react';
import Background from '../Atoms/Background';
import { Api } from '../dummyApi';

import {
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
  Image,
  ImageBackground,
  FlatList,
} from 'react-native';

const UserProgress = () => {
  const progress = Api.getProgress;
  return (
    <ImageBackground
      style={styles.background}
      source={require('../assets/images/gympic01.jpeg')}
    >
      <Background>
        <Text style={styles.demoApp}>Progress</Text>
        {/* <FlatList
          data={progress}
          renderItem={({}) => <Text>{progress.date}</Text>}
        /> */}
      </Background>
    </ImageBackground>
  );
};
const styles = StyleSheet.create({
  demoApp: {
    width: '100%',
    textAlign: 'center',
    padding: '10px',
    lineHeight: 40,
    fontSize: 40,
    top: 0,
  },
  input: {
    backgroundColor: 'white',
  },
  inputs: {
    top: 150,
    width: '100%',
  },
  background: {
    flex: 1,
  },
});

export default UserProgress;
