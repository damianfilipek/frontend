import { observer } from 'mobx-react-lite';
import React from 'react';
import { Pressable } from 'react-native';
import { Image, StyleSheet, ScrollView, View, Text, Button } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import Measurements from '../../models/Measurements';

const PhotoGallery = observer(() => {
  const { images } = Measurements.measurements;
  const { removeImage } = Measurements;
  return (
    <ScrollView style={styles.container}>
      <View style={styles.wrapper}>
        {images.map((data) => (
          <Pressable style={styles.imageWrapper}>
            <Image
              source={{ uri: data.uri }}
              key={data.id}
              style={{ width: 100, height: 100 }}
            />
            <RectButton style={styles.removeButton} onPress={() => removeImage(data)}>
              <Text
                style={{
                  fontSize: 18,
                  color: '#fff',
                  transform: [{ translateY: -3 }],
                }}
              >
                x
              </Text>
            </RectButton>
          </Pressable>
        ))}
      </View>
    </ScrollView>
  );
});

const styles = StyleSheet.create({
  container: {
    maxHeight: '30%',
  },
  wrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
  },
  imageWrapper: {
    paddingVertical: 15,
  },
  removeButton: {
    width: 18,
    height: 18,
    backgroundColor: 'red',
    borderRadius: 9,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 7,
    left: -7,
    zIndex: 999,
  },
});
export default PhotoGallery;
