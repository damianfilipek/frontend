import React from 'react';
import Measurements from '../../models/Measurements';
import MeasurementInputs from '../molecules/MeasurementInputs';
import { observer } from 'mobx-react-lite';
import { Animated, Text, ScrollView, StyleSheet } from 'react-native';
import { Swipeable, RectButton } from 'react-native-gesture-handler';
import { useEffect, useState } from 'react';
const { measurements, deleteMeasurement } = Measurements;

const rightSwipe = (data: any, progress: any, dragX: any) => {
  const trans = dragX.interpolate({
    inputRange: [-10, 10, 10, 11],
    outputRange: [0, 0, 100, 110],
  });

  return (
    <Animated.View
      style={{
        transform: [{ translateX: trans }],
      }}
    >
      <RectButton
        onPress={() => deleteMeasurement(data)}
        style={[styles.deleteButton]}
      >
        <Text style={styles.buttonName}>Delete</Text>
      </RectButton>
    </Animated.View>
  );
};

const MeasurementInputsMap = observer(() => {
  return (
    <ScrollView style={styles.container}>
      {measurements.data.map((data, i) => (
        <Swipeable
          renderRightActions={(progress, dragX) => rightSwipe(data, progress, dragX)}
          key={data.id}
        >
          <MeasurementInputs {...data} />
        </Swipeable>
      ))}
    </ScrollView>
  );
});

const styles = StyleSheet.create({
  container: {
    maxHeight: '60%',
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderBottomWidth: 1,
    paddingVertical: 10
  },
  deleteButton: {
    backgroundColor: 'red',
    width: 100,
    marginRight: 10,
    height: 35,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonName: {
    fontSize: 20,
    color: '#fff',
  },
});
export default MeasurementInputsMap;
