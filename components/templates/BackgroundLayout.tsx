import React from 'react';
import { ImageBackground } from 'react-native';

const BackgroundLayout = ({ children }: any) => {
  return (
    <ImageBackground
      source={require('../../assets/images/background.jpeg')}
      resizeMode="cover"
      style={{ width: '100%', height: '100%' }}
    >
      {children}
    </ImageBackground>
  );
};

export default BackgroundLayout;
