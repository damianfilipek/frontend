import React, { useState } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { observer } from 'mobx-react-lite';
import Measurements from '../../models/Measurements';

type Types = {
  name: string;
  value: string;
  id: string;
};
const MeasurementInputs = observer(({ name, value, id }: Types) => {
  const { changeName, changeValue } = Measurements;
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.inputName}
        placeholder="Measurement"
        value={name}
        onChangeText={(name) => changeName({ name, id })}
      />
      <TextInput
        style={styles.inputValue}
        placeholder="Value"
        keyboardType="decimal-pad"
        maxLength={6}
        value={value}
        onChangeText={(value) => changeValue({ value, id })}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
  },
  inputName: {
    width: 200,
    textAlign: 'center',
    height: 30,
    fontSize: 20,
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
  },
  inputValue: {
    textAlign: 'center',
    marginLeft: 30,
    width: 100,
    height: 30,
    fontSize: 20,
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
  },
});

export default MeasurementInputs;
