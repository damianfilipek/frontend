import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

type Props = {
  onPress: () => void;
};
const AddPhotoButton = ({ onPress }: Props) => {
  return (
    <RectButton style={styles.button} onPress={onPress}>
      <Text style={styles.text}>Add Photo</Text>
    </RectButton>
  );
};

const styles = StyleSheet.create({
  button: {
    width: 100,
    height: 40,
    backgroundColor: 'red',
    position: 'absolute',
    left: 40,
    bottom: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  text: {
    fontSize: 18,
    color: '#fff',
  },
});
export default AddPhotoButton;
