import { observer } from 'mobx-react-lite';
import React from 'react';
import { Pressable, Text, StyleSheet } from 'react-native';
import Measurements from '../../models/Measurements';


const AddButton = observer(() => {
  const {addMeasurement} = Measurements
  return (
    <Pressable style={styles.container} onPress={() => addMeasurement()}>
      <Text style={styles.text}>+</Text>
    </Pressable>
  );
})


const styles = StyleSheet.create({
  container: {
    width: 50,
    height: 50,
    alignItems: 'center',
    borderRadius: 50,
    backgroundColor: "red",
    position: 'absolute',
    bottom: 40,
    right:'50%', 
    transform: [{ translateX: 25 }]
  },
  text: {
    color: '#fff',
    lineHeight: 52,
    fontSize: 50,
  }
})
export default AddButton;
