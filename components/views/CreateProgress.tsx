import React from 'react';
import { View, TextInput, StyleSheet, Text, SafeAreaView } from 'react-native';
import AddButton from '../atoms/AddButton';
import MeasurementInputs from '../molecules/MeasurementInputs';
import { observer } from 'mobx-react-lite';
import Measurements from '../../models/Measurements';
import MeasurementInputsMap from '../organisms/MeasurementInputsMap';
import AddPhotoButton from '../atoms/AddPhotoButton';
import PhotoPicker from '../organisms/PhotoPicker';
import CreateProgressButton from '../atoms/CreateProgressButton';
import PhotoGallery from '../organisms/PhotoGallery';

const CreateProgress = observer(() => {
  return (
    <SafeAreaView style={styles.container}>
      <MeasurementInputsMap />
      <AddButton />
      <PhotoPicker />
      <CreateProgressButton onPress={() => console.log()} />
      <PhotoGallery />
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
});

export default CreateProgress;
