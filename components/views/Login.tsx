import * as React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
const screenHeight = Dimensions.get('window').height;

const Login = ({ navigation }: any) => {
  return (
    <ImageBackground
      source={require('../../assets/images/background.jpeg')}
      resizeMode="cover"
      style={{ width: '100%', height: '100%' }}
    >
      <View style={styles.container}>
        <View style={styles.loginWrapper}>
          <Text style={styles.title}>
            Fit
            <View style={styles.buttonDot}></View>
            Kingdom
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('CreateProgress')}
            style={styles.button}
          >
            <Image
              source={require('../../assets/images/googleIcon.png')}
              style={styles.icon}
            />
            <Text style={styles.buttonText}>Sign in with Google</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  title: {
    fontSize: 40,
    textAlign: 'center',
    color: '#fff',
    lineHeight: 0,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#fff',
    width: '60%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 19,
  },
  buttonDot: {
    backgroundColor: 'red',
    width: 10,
    height: 10,
    borderRadius: 10,
  },
  loginWrapper: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    height: screenHeight / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
